library ieee;
use ieee.std_logic_1164.all;

entity multiplekser is
  port(adres: in std_logic_vector(1 downto 0);
       wejscie: in std_logic_vector(3 downto 0);
       wyjscie: out std_logic);
end entity multiplekser;

architecture struct of multiplekser is

  component or4
    port(in1, in2, in3, in4 : in std_logic;
         out1: out std_logic);
  end component or4;

  component and3
    port(in1, in2, in3 : in std_logic;
         out1: out std_logic);
  end component and3;
  
  component inverter
    port(in1: in std_logic; out1: out std_logic);
  end component inverter;

  signal sy: std_logic_vector(1 downto 0);
  signal dy: std_logic_vector(3 downto 0);

begin

  not1 : inverter port map (in1 => adres(0), out1 => sy(0));
  not2 : inverter port map (in1 => adres(1), out1 => sy(1));
  a1 : and3 port map (in1 => wejscie(0), in2 => sy(1), in3 => sy(0), out1 => dy(0));
  a2 : and3 port map (in1 => wejscie(1), in2 => sy(1), in3 => adres(0), out1 => dy(1));
  a3 : and3 port map (in1 => wejscie(2), in2 => adres(1), in3 => sy(0), out1 => dy(2));
  a4 : and3 port map (in1 => wejscie(3), in2 => adres(1), in3 => adres(0), out1 => dy(3));
  y : or4 port map (in1 => dy(0), in2 => dy(1), in3 => dy(2), in4 => dy(3), out1 => wyjscie);

end architecture struct; 