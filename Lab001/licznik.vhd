library ieee;
use ieee.std_logic_1164.all;

entity licznik is
  port(disp1: out std_logic_vector(6 downto 0);
       disp2: out std_logic_vector(6 downto 0);
       clk: in std_logic;
       rst: in std_logic;
       clockEnable: in std_logic);
end entity licznik;

architecture struct of licznik is
  
  component seven_seg  -- arch: symulacja, synteza
    Port ( hex : in std_logic_vector(3 downto 0);
           seg : out std_logic_vector(6 downto 0));
  end component seven_seg;

  component licznik_u -- arch: behav
    GENERIC (
      N: integer:= 4; -- rozmiar licznika
      M: integer:= 9; -- warunek konca zliczania
      T: time:= 6 ns  -- czas propagacji
    );
    PORT (
      q: out std_logic_vector(N-1 downto 0); -- wyjscie danych
      rst: in std_logic; -- reset asynchroniczny, aktywny '1'
      ce: in std_logic;  -- clock enable
      clk: in std_logic  -- zegar
    );
  end component licznik_u;

  component and4
    port(in1, in2, in3, in4 : in std_logic;
         out1: out std_logic);
  end component and4;
  
  component inverter
     port(in1: in std_logic; out1: out std_logic);
  end component inverter;

  signal compareNum1Equals9, inv1, inv2 : std_logic;
  signal num1, num2 : std_logic_vector(3 downto 0);

begin

  licznik1: licznik_u port map(q=>num1, rst=>rst, ce=>clockEnable, clk=>clk);
  inverter1: inverter port map(in1=>num1(1), out1=>inv1);
  inverter2: inverter port map(in1=>num1(2), out1=>inv2);
  compare: and4 port map(in1=>num1(0), in2=>inv1, in3=>inv2, in4=>num1(3), out1=>compareNum1Equals9);
  licznik2: licznik_u port map(q=>num2, rst=>rst, ce=>compareNum1Equals9, clk=>clk);
  display1: seven_seg port map(hex=>num1, seg=>disp1);
  display2: seven_seg port map(hex=>num2, seg=>disp2);

end architecture struct;
