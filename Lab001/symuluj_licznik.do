#--------------------------------------------
# lab2: msim-vhdl
# makro symulacji dla plikow mux_bit.vhd i mux_slv.vhd
# przyklad uzycia polecen Tcl
#--------------------------------------------
if {$argc < 2} {
    puts "Podaj nazwe pliku .vhd i nazwe jednostki projektowej!"
    puts "Skladnia: do symuluj.do nazwa_pliku nazwa_jeednostki_projektowej"
    abort
}

#--------------------------------------------
# procedura generacji wymuszenia dla wektora
# sygnal - nazwa sygnalu forsowanego
# zakres - ilosc krokow wymuszen
# czas trwania kroku z jednostka (np. "10 ns")
proc forsuj {sygnal zakres czas} {
 set x 0
 for {set i 0} {$i < $zakres} {incr i} {
   set x [expr $x+1]
   force $sygnal 10#$x
   run $czas
 }
}
#--------------------------------------------

set plik $1
set jednostka $2

vcom -work work -2002 -explicit $plik
vsim work.$jednostka
view wave -title $jednostka
#
add wave clk
add wave disp1
add wave num1
add wave disp2
add wave num2
#
force rst "2#1" 
force rst "2#0" 6
force clockEnable "2#1"
forsuj clk 250 "6 ns"
#
update
WaveRestoreZoom {0 ns} $Now