if {$argc < 2} {
    puts "U?yj sk?adni:"
    puts "do bin2onehot.do <plik do oczytu> <plik do zapisu>"
    abort
}

vcom bin2onehot.vhd
vcom bin2onehot_tb.vhd
vsim work.bin2onehot_tb(testbench_arch) -Ginput=$1 -Goutput=$2
add wave -position insertpoint sim:/bin2onehot_tb/*
run -all