if {$argc < 2} {
    puts "Uzyj skladni:"
    puts "do ram20d.do <plik do oczytu> <plik do zapisu>"
    abort
}

vcom ram20d.vhd
vcom ram20d_tb.vhd
vsim work.ram20d_tb(testbench_arch) -Ginput=$1 -Goutput=$2
add wave -position insertpoint sim:/ram20d_tb/*
run -all
