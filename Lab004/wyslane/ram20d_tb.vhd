LIBRARY  IEEE;

USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE IEEE.STD_LOGIC_SIGNED.all;
USE IEEE.NUMERIC_STD.all;
USE STD.TEXTIO.ALL;

ENTITY ram20d_tb IS
  GENERIC (
    input, output: STRING
  );
END ram20d_tb;

ARCHITECTURE testbench_arch OF ram20d_tb IS
  
  COMPONENT ram20d
		PORT (
			clk  : in std_logic;
      we   : in std_logic;
      oe   : in std_logic;
      addr : in std_logic_vector(5 downto 0);
      data : inout std_logic_vector(15 downto 0)
		);
	END COMPONENT;

	SIGNAL t_clk  : std_logic := '0';
	SIGNAL t_we   : std_logic := '1';
	SIGNAL t_oe   : std_logic := '0';
	SIGNAL t_addr : std_logic_vector(5 downto 0);
	SIGNAL t_data : std_logic_vector(15 downto 0);
  SIGNAL sData : std_logic_vector(15 downto 0);
  SIGNAL rData : std_logic_vector(15 downto 0);
	SIGNAL endOfFile : std_logic := '0';
	SIGNAL inputRows : integer := 0;
	SIGNAL outputRows : integer := 0;
 
  constant simTime : time := 600 ns;

  FILE infile: TEXT open READ_MODE is input;
  FILE outfile: TEXT open WRITE_MODE is output;

BEGIN
  UUT : entity work.ram20d
  PORT MAP (
		clk  => t_clk,
		we   => t_we,
		oe   => t_oe,
		addr => t_addr,
		data => t_data
	);

  clockProcess: PROCESS
  BEGIN
   WAIT FOR simTime/200;
   t_clk <= not t_clk;
  END PROCESS clockProcess;

  generator: PROCESS(t_clk)
    VARIABLE address: integer := 0;
  BEGIN
    if (t_we = '1' and t_oe = '0') or (t_we = '0' and t_oe = '1') then
      if (t_clk'event and t_clk = '1') or t_oe = '1' then
        t_addr <= conv_std_logic_vector(address, 6);
        address := address + 1;
      end if;
    elsif t_data = "ZZZZZZZZZZZZZZZZ" then
      address := 0;
    end if;
  END PROCESS generator;

  simulation: PROCESS
  BEGIN
    wait for simTime;
    ASSERT false REPORT "End of simulation at time " & time'image(now) SEVERITY Failure;
  END PROCESS simulation;

  enableRead: PROCESS(endOfFile)
  BEGIN
    if (endOfFile'event and endOfFile = '1') then
      t_we <= not t_we AFTER simTime/100;
      t_oe <= not t_oe AFTER simTime/100;
    end if;
  END PROCESS enableRead;

  writeData: PROCESS(t_clk)
  BEGIN
    if (t_clk'event and t_clk = '1' and t_we = '1') then
      t_data <= sData;
    end if;
  END PROCESS writeData;

  readData: PROCESS(t_clk)
    VARIABLE text_out: LINE;
  BEGIN
    if t_oe = '1' and inputRows > outputRows then
      rData <= t_data;
      if not (rData = "ZZZZZZZZZZZZZZZZ") and not (rData = "UUUUUUUUUUUUUUUU") then
        write(text_out, conv_integer(rData));
        writeline(outfile, text_out);
        outputRows <= outputRows + 1;
      end if;
    end if;
  END PROCESS readData;

  PROCESS
    VARIABLE text_in : LINE;
    VARIABLE in_data : integer := 0;
  BEGIN
    saveToRam: while not Endfile(infile) loop
      if t_we = '1' then
        readline(infile, text_in);
        read(text_in, in_data);
        sData <= conv_std_logic_vector(in_data, 16); 
        inputRows <= inputRows + 1;
      end if;
      WAIT FOR simTime/100;
    end loop saveToRam;
    sData <= "ZZZZZZZZZZZZZZZZ";
    endOfFile <= '1';
    WAIT;
  END PROCESS;

END testbench_arch;