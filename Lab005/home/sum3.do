if {$argc < 2} {
    puts "Uzyj skladni:"
    puts "do sum3.do <liczba bitow> <nazwa pliku z wynikami>"
    abort
}

vcom sum3.vhd
vcom sum3_tb.vhd
vsim work.sum3_tb(testbench_arch) -Gliczba=$1 -Goutput=$2
add wave -position insertpoint sim:/sum3_tb/*
run -all