LIBRARY  IEEE;

USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE STD.TEXTIO.ALL;

ENTITY sum3_tb IS
  GENERIC (
    liczba: integer
  );
END sum3_tb;	

ARCHITECTURE testbench_arch OF sum3_tb IS
  COMPONENT adderN
   	GENERIC (
   	  N: integer:=4
 	  );
    PORT (
      ai,bi:	in std_logic_vector(liczba-1 downto 0):=(others=>'0');
	    ci:	in std_logic;
     	so:	out std_logic_vector(liczba-1 downto 0);
     	co:	out std_logic
    );
  END COMPONENT;
  
  SIGNAL t_ai, t_bi: std_logic_vector(liczba-1 downto 0):=(others=>'0');
  SIGNAL t_ci: std_logic := '0';
  SIGNAL t_so: std_logic_vector(liczba-1 downto 0);
  SIGNAL t_co: std_logic;

BEGIN
  UUT: adderN
  GENERIC MAP (
    N => liczba
  )
  PORT MAP (
    ai => t_ai,
    bi => t_bi,
    ci => t_ci,
    so => t_so,
    co => t_co
  );
  
  PROCESS
  BEGIN
    aiLoop: for i in 0 to (2**liczba)-1 loop
      t_ai <= conv_std_logic_vector(i, liczba);
      biLoop: for j in 0 to (2**liczba)-1 loop
        t_bi <= conv_std_logic_vector(j, liczba);
        wait for 6 ns;
      end loop biLoop;
    end loop aiLoop;
    WAIT;
  END PROCESS;
END testbench_arch;