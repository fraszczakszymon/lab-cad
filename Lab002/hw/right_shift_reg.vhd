library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity right_shift_reg is
   Port    (clk : in std_logic;
             d : in std_logic;
             q : out std_logic_vector(7 downto 0));
end entity right_shift_reg;

architecture right_double_reg of right_shift_reg is
constant Tpd: time := 3 ns;
signal q_i: std_logic_vector(7 downto 0);

begin
process(clk)
	begin
	  if clk='1' and clk'event then
		  q_i <= d & q_i(7 downto 1) after Tpd;
	  end if;	
	end process;
  q <= q_i;
end architecture right_double_reg;
