vsim work.shift_reg_tb(behav)
add wave -position insertpoint sim:/shift_reg_tb/*
run 450
dataset save sim double_reg.wlf
quit -sim

vsim work.right_shift_reg_tb(behav)
add wave -position insertpoint sim:/right_shift_reg_tb/*
run 450
dataset save sim right_double_reg.wlf
quit -sim

dataset open double_reg.wlf sim1
dataset open right_double_reg.wlf sim2

compare start sim1 sim2
compare add sim1:/q_i sim2:/q_i
compare run
compare info -write simInfo.txt
compare end

dataset close sim1
dataset close sim2