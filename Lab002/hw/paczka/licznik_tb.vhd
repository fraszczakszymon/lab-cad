library ieee;
use ieee.std_logic_1164.all;

entity licznik_tb is
end entity licznik_tb;

architecture behav of licznik_tb is
   signal disp1_i : std_logic_vector(6 downto 0);
   signal disp2_i : std_logic_vector(6 downto 0);
   signal clk_i : std_logic := '0';
   signal rst_i : std_logic := '0';
   signal clockEnable_i : std_logic := '0';
 
   constant simTime : time := 650 ns;
 
begin
  uut: entity work.licznik port map (
    clk => clk_i,
    rst => rst_i,
    clockEnable => clockEnable_i,
    disp1 => disp1_i,
    disp2 => disp2_i
  );

  clk_i_process : process
  begin
    wait for 6 ns;
    clk_i <= not clk_i;
  end process;
  rst_i <= '1','0' after 6 ns;
  clockEnable_i <= '1';
 
 
  sim_end_process: process
  begin
    wait for simTime;
    assert false
      report "End of simulation at time " & time'image(now)
      severity Failure;
  end process sim_end_process;
 
end architecture behav;
